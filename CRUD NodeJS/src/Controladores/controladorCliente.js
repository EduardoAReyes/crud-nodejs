const controlador = {};

controlador.listar = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM cliente', (err, clientes) => {
            if(err){
                res.json(err);
            }
            console.log(clientes)
            res.render('cliente', {
                data: clientes
            });
        });
    });
};

controlador.registrar = (req, res) => {
    const datos = req.body;
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO cliente SET ?',[datos], (error, cliente) => {
            console.log(cliente);
            res.redirect('/');
        });
    });
};

controlador.eliminar = (req, res) => {
    console.log(req.params.pk_id_cliente);
    const {pk_id_cliente} = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM cliente WHERE pk_id_cliente = ?', [pk_id_cliente], (err,cliente) => {
            res.redirect('/')
        });
    });
};

controlador.actualizar = (req, res) => {
    
};

module.exports = controlador;