const express = require ('express');
const ruta = express.Router();

const controladorCliente = require('../Controladores/controladorCliente');

// ruta.post('/add', controladorCliente.actualizar
ruta.post('/add', controladorCliente.registrar);
ruta.get('/', controladorCliente.listar);
ruta.get('/eliminar/:pk_id_cliente', controladorCliente.eliminar);

module.exports = ruta;