const express = require('express');
const path = require('path');
const app = express();
const morgan = require('morgan');
const mariadb = require('mariadb');
const mysql = require('mysql');
const miConexion = require('express-myconnection');
const host = 'localhost'
const puerto = '3307'
const usuario = 'root'
const contraseña = 'Maria1234000'
const db = 'bibliotecajs'


const rutasCliente = require('./Rutas/clientes');

//Settings
app.set('port', process.env.PORT || 3000)
app.set('view engine', 'ejs');
app.set('Vistas', path.join(__dirname, 'Vistas'));

//Middlewares
app.use(morgan('dev'));
app.use(miConexion(mysql, {
    host: host,
    port: puerto,
    user: usuario,
    password: contraseña,
    database: db
}, 'single'))
app.use(express.urlencoded({extended: false}));

//Rutas
app.use('/', rutasCliente);

//Documentos estaticos
app.use(express.static(path.join(__dirname, 'public')))


app.listen(app.get('port'),() => {
    console.log('Servidor en 3000')
});

